using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableCoin : MonoBehaviour
{
    [SerializeField]
    private int _value;
    private void Start()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        GestionInventory player;
        if ((player = other.gameObject.GetComponent<GestionInventory>()) != null)
        {
            player.AddCoins(_value);
            DestroyItem();
        }
    }
    private void DestroyItem()
    {
        Destroy(gameObject);
    }
}
