public static class LibraryEnums 
{
    public enum Escenas
    {
        GameScreen,
        GameOverScreen
    }

    public enum GameFinish
    {
        Win,
        Lose
    }
}
