using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LibraryGameObjetcs : MonoBehaviour
{
    private static LibraryGameObjetcs _instance;
    public static LibraryGameObjetcs Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("Library is NULL");
            }
            return _instance;
        }
    }
    public GestionInventory GestionInventory;
    private void Awake()
    {
        if (_instance != null)
            Destroy(gameObject);
        else
        {
            _instance = this;
        }
        GestionInventory = GameObject.Find("PlayerArmature").GetComponent<GestionInventory>();
    } 
}
