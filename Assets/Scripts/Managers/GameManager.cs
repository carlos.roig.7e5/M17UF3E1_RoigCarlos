using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using static LibraryEnums;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;

    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("Game Manager is NULL");
            }
            return _instance;

        }
    }
    public Escenas _scene;
    public GameFinish _gameFinish;
    private bool _calledStartGame;
    private int _actualNumberOfCoins;
    private int _totalNumberOfCoins;
    [SerializeField]
    private InventorySO _inventorySO;
    private GameObject _player;
    private GameObject _spawnerController;
    [SerializeField]
    private GameObject _playerSpawnPoint;
    private void Awake()
    {
        if (_instance != null)
            Destroy(gameObject);
        else
        {
            DontDestroyOnLoad(gameObject);
            _instance = this;
        }
    }
    void Start()
    {
        DontDestroyOnLoad(gameObject);
        _calledStartGame = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!_calledStartGame)
        {
            _calledStartGame = true;
            _player = GameObject.Find("PlayerArmature");
            _spawnerController = GameObject.Find("CoinSpawner");
            _player.GetComponent<GestionInventory>().OnGrabCoin += OnCoinColected;
            _inventorySO.Coins = 0;
            _totalNumberOfCoins = _spawnerController.GetComponent<CoinSpawnController>().CoinPosition.Length;
            OnCoinColected();
            _player.GetComponent<CharacterController>().enabled = false;
            _player.transform.position = _playerSpawnPoint.transform.position;
            _player.GetComponent<CharacterController>().enabled = true;
        }
    }

    void OnCoinColected()
    {
        _actualNumberOfCoins = _inventorySO.Coins;
        UIManager.Instance.UpdateCoins(_actualNumberOfCoins,_totalNumberOfCoins);
        if (_actualNumberOfCoins >= _totalNumberOfCoins)
            GameOver();
        else
            _spawnerController.GetComponent<CoinSpawnController>().SpawnCoin();
    }

    void GameOver()
    {
        _calledStartGame = false;
    }
}
