using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GestionInventory : MonoBehaviour
{
    [SerializeField]
    private InventorySO _inventorySO;
    public delegate void GrabCoin();
    public event GrabCoin OnGrabCoin;
    // Start is called before the first frame update
    void Start()
    {
    }

    public void AddCoins(int number)
    {
        _inventorySO.Coins += number;
        OnGrabCoin();
    }

    public int ReturnCoins()
    {
        return _inventorySO.Coins;
    }

    public void StartCoins(int value)
    {
        _inventorySO.Coins = value;
    }
}
