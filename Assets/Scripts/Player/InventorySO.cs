using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Inventory", menuName = "InventorySO")]
public class InventorySO : ScriptableObject
{
    public int Coins;
}
