using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinSpawnController : MonoBehaviour
{
    [SerializeField]
    private Vector3[] _coinPosition;
    public Vector3[] CoinPosition
    {
        get { return _coinPosition; }
    }
    private int _counter = 0;
    [SerializeField]
    private GameObject _coin;
   public void SpawnCoin()
    {
        if (_counter >= _coinPosition.Length)
        {
            _counter = 0;
        }
        Instantiate(_coin, _coinPosition[_counter], Quaternion.identity);
        _counter += 1;
    }
}
